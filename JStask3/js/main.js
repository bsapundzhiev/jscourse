/**
 * App goes Here
 * @param  {Object} GLOBAL
 * @return {Void}
 */
(function (GLOBAL){
   'use strict';

    GLOBAL.app = GLOBAL.app || {};
    GLOBAL.winTemplate = [];
    app.windowList= [];
    app.taskList= [];

    //Main ViewModel
    var ViewModel = {
        activeModel: null,
        wmodels: []
    };
    
    //VewModel Commands
    ViewModel.addNewWindow = function() {
        //Create new window model
        var wm = new WindowViewModel();
        //minimize active window if set
        if(this.activeModel) {
            pubsub.publish("minimizeWindow", this.activeModel.windowModel);
        }
        //Store the active window ViewModel
        this.activeModel = wm;
        app.addNewWindowView();
        wm.subscribe();
        ViewModel.wmodels.push(wm);
    };

    ViewModel.destroyWindow = function () {
        
        pubsub.publish("destroyWindow", this.activeModel.windowModel);
        this.wmodels = _.without(this.wmodels, this.activeModel);
        delete this.activeModel;
    };

    ViewModel.minimizeWindow = function() {

        pubsub.publish("minimizeWindow", this.activeModel.windowModel);
    };

    ViewModel.restoreWindow = function(wid) {
        //minimize active window
        if(this.activeModel && wid !== this.activeModel.windowModel.id) {
            pubsub.publish("minimizeWindow", this.activeModel.windowModel);
        }
        //switch active window model
        this.activeModel = _.find(this.wmodels, function(m) {
            if(m.windowModel.id === wid) {
                return true;
            }
        });
        console.assert(this.activeModel,"activeModel not found");
        pubsub.publish("restoreWindow", this.activeModel.windowModel);
    };

    app.init = function() {
        this.taskbar = new UITaskBar(ViewModel);
    };

    //TODO: move to taskbar view or create window service ?
    app.addNewWindowView = function () {

        var win = new UIWindow(ViewModel);
        var task = new UITaskBarTask(ViewModel);

        app.windowList.push(win);
        app.taskList.push(task);
    };
    //Load templates
    app.preloadResources = function (templates) {
        templates.forEach(function(template, index, array) {
            var client = new XMLHttpRequest();
            client.open('GET', template);
            client.onreadystatechange = function() {
                if(client.readyState === 4) {
                    GLOBAL.winTemplate.push(client.responseText);
                    if(index === array.length - 1){
                       app.init();
                    }
                }
            }
            client.send();
        });
    };

    app.preloadResources(['templates/window.html', 'templates/taskbar.html', 'templates/task.html']);

})(window);