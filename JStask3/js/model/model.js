/**
 * Window Data Model
 * @param  {Object} GLOBAL
 * @return {Void}
 */
(function (GLOBAL) {
    'use strict';
    
    GLOBAL.Model = function () {
        this.id = _.uniqueId('win_');
        this.title = "Win Title";
        this.text = "Window Text";
        this.fulltitle = this.title.concat(" [").concat(this.id).concat("] ");

        this.canExecute = function(obj) {
            return (obj && obj.id === this.id);
        };
    }

})(window);