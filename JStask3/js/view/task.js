/**
 * Task(Icon) View
 * @param  {Object} GLOBAL
 * @return {Void}
 */
(function (GLOBAL) {
    'use strict';

   	GLOBAL.UITaskBarTask = function (viewModel) {
        this.model = viewModel;
        this.windowModel = viewModel.activeModel.windowModel;
        this.id = this.windowModel.id;

        viewModel.activeModel.destroyTask = this.destroy.bind(this);
        this.elements = {};
        this.render();
        this.attachEvents();
    };

    UITaskBarTask.prototype.attachEvents = function () {
        this.elements.task.addEventListener("click", this.model.restoreWindow.bind(this.model, this.id));
    };

    UITaskBarTask.prototype.dettachEvents = function () {
        this.elements.task.removeEventListener("click", this.model.restoreWindow);

    };

    UITaskBarTask.prototype.render = function () {

		var tpl = _.template(GLOBAL.winTemplate[2]);
 		this.rootEl = document.createElement('li');
        this.rootEl.innerHTML =  tpl(this.model);
        document.querySelector('.nav').appendChild(this.rootEl);
        this.elements.task = this.rootEl.querySelector('.glyphicon');
    };


    UITaskBarTask.prototype.destroy = function (topic, winmodel) {
        if(!winmodel.canExecute(this)) {
            return;
        }
        this.dettachEvents();
        this.rootEl.remove();
    };

})(window);