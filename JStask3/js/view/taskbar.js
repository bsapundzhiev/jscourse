/**
 * TaskBar View
 * @param  {Object} GLOBAL
 * @return {Void}
 */
(function (GLOBAL) {
    'use strict';

    GLOBAL.UITaskBar = function (viewModel) {
        this.id = _.uniqueId('uib_');
        this.model = viewModel;
        this.elements = {};
        this.render();
        this.attachEvents();
    }

    UITaskBar.prototype.attachEvents = function () {
        
        this.elements.task.addEventListener("click", this.model.addNewWindow.bind(this.model));
    };

    UITaskBar.prototype.dettachEvents = function () {

        this.elements.task.removeEventListener("click", this.model.addNewWindow);
    };

    UITaskBar.prototype.render = function () {

        this.rootEl =  document.querySelector('.navbar');
        this.elements.task = this.rootEl.querySelector('.glyphicon');
    };

    UITaskBar.prototype.destroy = function () {

        this.dettachEvents();
        this.rootEl.remove();
        delete this.rootEl;
    };

})(window);