/**
 * Window View
 * @param  {Object} GLOBAL
 * @return {Void}
 */
(function (GLOBAL) {
    'use strict';

    GLOBAL.UIWindow = function (viewModel) {

        this.model = viewModel;
        this.windowModel = viewModel.activeModel.windowModel;
        this.id = this.windowModel.id;

        viewModel.activeModel.destroyWindow = this.destroy.bind(this);
        viewModel.activeModel.minimizeWindow = this.minimize.bind(this);
        viewModel.activeModel.restoreWindow = this.restore.bind(this);
        

        this.elements = {};
        this.render();
        this.attachEvents();
    }

    UIWindow.prototype.attachEvents = function () {

        this.elements.btnClose.addEventListener("click", this.model.destroyWindow.bind(this.model));
        this.elements.btnMaximize.addEventListener("click", this.maximize.bind(this));
        this.elements.btnMinimize.addEventListener("click", this.model.minimizeWindow.bind(this.model));
    };

    UIWindow.prototype.dettachEvents = function () {

        this.elements.btnClose.removeEventListener("click", this.model.destroyWindow);
        this.elements.btnMaximize.removeEventListener("click", this.maximize);
        this.elements.btnMinimize.removeEventListener("click", this.model.minimizeWindow);

    };

    UIWindow.prototype.render = function () {

        var tpl = _.template(GLOBAL.winTemplate[0]);
        this.rootEl = document.createElement('div');
        this.rootEl.innerHTML =  tpl(this);
        this.elements.placeHolder =  document.getElementById("desktop");
        this.elements.btnClose = this.rootEl.querySelector('.win-close');
        this.elements.btnMaximize = this.rootEl.querySelector('.win-maximize');
        this.elements.btnMinimize = this.rootEl.querySelector('.win-minimize');
        this.elements.window = this.rootEl.querySelector('.modal');
        this.elements.windowBody = this.rootEl.querySelector('.modal-body');
        this.elements.placeHolder.appendChild(this.rootEl);
    };

    UIWindow.prototype.destroy = function (topic, winmodel) {
        if(!winmodel.canExecute(this)) {
            return;
        }

        this.dettachEvents();
        this.rootEl.remove();
        delete this.rootEl;
    };

    UIWindow.prototype.minimize = function (topic, winmodel) {
        if(!winmodel.canExecute(this)) {
            return;
        }
        this.elements.window.classList.remove("show");
    };

    UIWindow.prototype.restore = function (topic, winmodel) {
        if(!winmodel.canExecute(this)) {
            return;
        }
        this.elements.window.classList.add("show");
    };

    UIWindow.prototype.maximize = function () {
        this.elements.windowBody.classList.toggle("normal");
        this.elements.windowBody.classList.toggle("maximized");
    };

})(window);