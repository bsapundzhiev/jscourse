(function (GLOBAL) {
    'use strict';

    //Window ViewModel
    GLOBAL.WindowViewModel = function() {

        this.windowModel = new Model();

        //window view events
        this.subscribe = function() {
            pubsub.subscribe("destroyWindow", this.destroyWindow);
            pubsub.subscribe("destroyWindow", this.destroyTask);
            pubsub.subscribe("minimizeWindow", this.minimizeWindow);
            pubsub.subscribe("restoreWindow", this.restoreWindow);
        };
    };

})(window);